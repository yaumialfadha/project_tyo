[![pipeline status](https://gitlab.com/ZoneZ/test-of-pepew/badges/master/pipeline.svg)](https://gitlab.com/ZoneZ/test-of-pepew/commits/master)
[![coverage report](https://gitlab.com/ZoneZ/test-of-pepew/badges/master/coverage.svg)](https://gitlab.com/ZoneZ/test-of-pepew/commits/master)
[![built with Django](https://img.shields.io/badge/build%20with-Django-green.svg)](https://www.djangoproject.com/)
[![built with Python3](https://img.shields.io/badge/build%20with-Python3-blue.svg)](https://www.python.org/)

Heroku Link : http://ppw-c-testing-goat-lair.herokuapp.com/