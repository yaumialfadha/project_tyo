import json
import requests

from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt

response = {}


def book_list(request):
    response['username'] = []
    if 'username' in request.session.keys():
        response['username'] = request.session['username']

    response['favorite_books'] = []
    if 'books' in request.session.keys():
        response['favorite_books'] = request.session['books']

    if request.method == 'POST':
        search_key = request.POST.get('search')
        books = get_book_json(search_key)
        response['books'] = books
    else:
        books = get_book_json("quilting")
        response['books'] = books
    return render(request, 'book-list.html', response)


def get_book_json(category):
    data = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + category).json()
    return data['items']
    # result = data['items']
    # return_data = {'data': result}
    # json_data = json.dumps(return_data)
    # return HttpResponse(json_data, content_type='application/json')


@csrf_exempt
def token_verification(request):
    token = request.POST['token']
    VERIFY_TOKEN_URL = 'https://www.googleapis.com/oauth2/v3/tokeninfo?id_token='
    data = requests.get(VERIFY_TOKEN_URL + token).json()
    request.session['access_token'] = token
    request.session['username'] = data['name']
    request.session['profile_image_url'] = data['picture']
    request.session['email'] = data['email']
    return HttpResponse(json.dumps({'log': 'Success'}),
                        content_type='application/json')


def add_session_favorite(request, book_id):
    if 'books' not in request.session.keys():
        request.session['books'] = [book_id]
    else:
        books = request.session['books']
        if book_id not in books:
            books.append(book_id)
            request.session['books'] = books
    return HttpResponseRedirect(reverse('book-list:book-list'))


def delete_session_favorite(request, book_id):
    books = request.session['books']
    books.remove(book_id)
    request.session['books'] = books
    return HttpResponseRedirect(reverse('book-list:book-list'))


def clear_session(request):
    request.session.flush()
    return HttpResponse(json.dumps({'log': 'Success'}),
                        content_type='application/json')