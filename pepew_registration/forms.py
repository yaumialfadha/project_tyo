from django import forms

from .models import Subscriber


class SubscriberForm(forms.ModelForm):

    password1 = forms.CharField(
        label='Password',
        strip=False,
        widget=forms.PasswordInput(attrs={'class': 'form-control'}),
        help_text='Minimal 8 karakter, maksimal 128 karakter. Tidak boleh sepenuhnya berupa angka.',
        required=True,
    )

    password2 = forms.CharField(
        label='Konfirmasi Password',
        strip=False,
        widget=forms.PasswordInput(attrs={'class': 'form-control'}),
        help_text='Masukkan password yang sama untuk verifikasi',
        required=True,
    )

    def __init__(self, *args, **kwargs):
        super(SubscriberForm, self).__init__(*args, **kwargs)

        name_field = self.fields['name']
        name_field.label = 'Komentar'
        name_field.widget = forms.Textarea(attrs={'class': 'form-control',
                                                'placeholder': 'Masukkan status Anda.',
                                                'rows': 5,
                                                'cols': 8}
 )
        name_field.required = True
        name_field.error_messages = {
            'max_length': 'Panjang maksimal nama adalah 25 karakter.',
            'required': 'Nama tidak boleh kosong.',
        }

        email_field = self.fields['email']
        email_field.widget = forms.EmailInput(attrs={'class': 'form-control',
                                                     'placeholder': 'Masukkan e-mail Anda'})

    def clean_password2(self):
        password1 = self.cleaned_data['password1']
        password2 = self.cleaned_data['password2']

        if password1 and password2:
            if password1 != password2:
                raise forms.ValidationError(
                    'Kedua password tidak sama.',
                    code='password_mismatch',
                )
            elif len(password1) > 128 or len(password2) > 128:
                raise forms.ValidationError(
                    'Panjang maksimal password adalah 128 karakter.',
                    code='max_length',
                )

        if len(password2) < 8:
            raise forms.ValidationError(
                'Password kurang dari 8 karakter.',
                code='minimum_character_violation',
            )

        if password2.isdigit():
            raise forms.ValidationError(
                'Password tidak boleh sepenuhnya berupa angka.',
                code='numeric_password_violation',
            )
        return password2

    def save(self, commit=True):
        subscriber = super().save(commit=False)
        subscriber.password = self.cleaned_data['password1']
        if commit:
            subscriber.save()
        return subscriber

    class Meta:
        model = Subscriber
        fields = ['name', 'email']