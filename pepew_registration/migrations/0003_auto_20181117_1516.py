# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2018-11-17 08:16
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pepew_registration', '0002_auto_20181117_0048'),
    ]

    operations = [
        migrations.RenameField(
            model_name='subscriber',
            old_name='username',
            new_name='name',
        ),
    ]
