from django.db import models


class Subscriber(models.Model):
    name = models.CharField(max_length=200)
    email = models.EmailField(unique=True)
    title = models.CharField(max_length = 200,default="", editable=True)
    image = models.URLField(max_length = 200, blank = True, null =True, default="", editable=True)
    news = models.CharField(max_length = 200, default="", editable=True)
    

    def __str__(self):
        return self.name

    
