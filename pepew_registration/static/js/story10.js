function validateInput() {
    if (
        $('#id_name').val() && $('#id_email').val() &&
        $('#id_password1').val() && $('#id_password2').val().length > 0 && emailIsValidated
    ) $('input[type=submit]').prop('disabled', false);
    else $('input[type=submit]').prop('disabled', true);
}

$(document).ready(function () {
    validateInput();

    $(document).on('keyup', '#id_name, #id_email, #id_password1, #id_password2', function () {
        validateInput();
    });

    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader('X-CSRFToken', csrftoken);
            }
        }
    });

    $('#id_email').on({
        'change': function () {
            var email = $(this).val();
            $.ajax({
                url: validateEmailURL,
                data: {'email': email},
                dataType: 'json',
                success: function (result) {
                    if (result.is_taken) {
                        $('#id_email').after(
                            "<div class='alert alert-danger alert-dismissible fade show' role='alert'>" +
                            result.message + "<button type='button' " +
                            "class='close' data-dismiss='alert' aria-label='Close'>" +
                            "<span aria-hidden='true'>&times;</span></button>" + "</div>");
                    } else {
                        $('#id_email').after(
                            "<div class='alert alert-success alert-dismissible fade show' role='alert'>" +
                            result.message + "<button type='button' " +
                            "class='close' data-dismiss='alert' aria-label='Close'>" +
                            "<span aria-hidden='true'>&times;</span></button>" + "</div>");
                    }
                },
            }).then(function (result) {
                emailIsValidated = !(result.is_taken);
                validateInput();
            });
        },
    });

    $('#add-subscriber').on('submit', function () {
        var name = $('#id_name');
        var email = $('#id_email');
        var password1 = $('#id_password1');
        var password2 = $('#id_password2');
        var data = {
            'name': name.val(),
            'email': email.val(),
            'password1': password1.val(),
            'password2': password2.val(),
        };
        $.ajax({
            method: 'POST',
            url: submitFormURL,
            type: 'json',
            data: data,
            success: function (result) {
                if (result.success) {
                    $('input[type=submit]').after("<div class='alert alert-success " +
                        "alert-dismissible fade show' role='alert'>" +
                            result.log + "<button type='button' class='close' " +
                        "data-dismiss='alert' aria-label='Close'>" +
                            "<span aria-hidden='true'>&times;</span></button>" + "</div>");
                } else {
                    var error_message;
                    for (var key in result.log) {
                        if (key in {name: 0, email: 0, password1: 0, password2: 0}) {
                            error_message = result.log[key][0]
                        }
                    }
                    $('input[type=submit]').after("<div class='alert alert-danger " +
                        "alert-dismissible fade show' role='alert'>" +
                            error_message + "<button type='button' class='close' " +
                        "data-dismiss='alert' aria-label='Close'>" +
                            "<span aria-hidden='true'>&times;</span></button>" + "</div>");
                }
            }
        }).then(function (result) {
            if (result.success) {
                name.val("");
                email.val("");
                password1.val("");
                password2.val("");
            }
            $('#subscriber-table').DataTable().ajax.reload();
        });
        event.preventDefault();
    });

});

function csrfSafeMethod(method) {
    return(/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}