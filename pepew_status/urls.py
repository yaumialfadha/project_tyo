from django.conf.urls import url

from .views import index, home2, kebakaran, berita_1, berita_2, profile, add_status, kebakaran_1, banjir_1, cuaca_ekstrim_1

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add-status/$', add_status, name='add_status'),
    url(r'^profile/$', profile, name='profile'),
    url(r'^home-2/', home2, name='home2'),
    url(r'^kebakaran/', kebakaran, name='kebakaran'),
    url(r'^berita-1/', berita_1, name='berita_1'),
    url(r'^berita-2/', berita_2, name='berita_2'),
    url(r'^kebakaran-1/', kebakaran, name='kebakaran_1'),
    
    
]
