from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse


from .models import News_1, all_news_1, News_2, all_news_2

response = {}


def index(request):
    response['status'] = Status.objects.all()
    response['form'] = StatusForm()
    response['username'] = []
    if 'username' in request.session.keys():
        response['username'] = request.session['username']
    html = 'index.html'
    return render(request, html, response)


def add_status(request):
    if request.method == 'POST':
        form = StatusForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('status:index'))
    else:
        form = StatusForm()
    return render(request, 'index.html', {'form': form,
                                          'status': Status.objects.all()})


def profile(request):
    response = {'news' : all_news_1.objects.all()}
    return render(request, 'profile.html', response)

def home2(request):
    response = {'news' : all_news_2.objects.all()}
    return render(request, 'home-2.html', response)

def kebakaran(request):
    return render(request, 'kebakaran-1.html')
def berita_1(request):
    response = {'news' : News_1.objects.all()}
    return render(request, 'all_news.html', response)
def berita_2(request):
    response = {'news' : News_2.objects.all()}
    return render(request, 'all_news_2.html', response)

def kebakaran_1(request):
    return render(request, 'kebakaran-12.html')
def banjir_1(request):
    return render(request, 'banjir-12.html')
def cuaca_ekstrim_1(request):
    return render(request, 'cuaca-ekstrim-12.html')
# def all_news(request):
     
#     return render(request, 'all_news.html', response)




