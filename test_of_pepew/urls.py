"""test_of_pepew URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic import RedirectView

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^status/', include(('pepew_status.urls', 'status'), namespace='status')),
    url(r'^$', RedirectView.as_view(url='/profile/')),
    url(r'^profile/$', RedirectView.as_view(url='/status/profile/')),
    url(r'^saran/', include(('pepew_registration.urls', 'registration'), namespace='registration')),
   
    url(r'^home-2/',RedirectView.as_view(url='/status/home-2/')),
    url(r'^home-3/',RedirectView.as_view(url='/status/home-3/')),
    url(r'^home-4/',RedirectView.as_view(url='/status/home-4/')),
    url(r'^kebakaran/',RedirectView.as_view(url='/status/kebakaran/')),
    url(r'^berita-1/',RedirectView.as_view(url='/status/berita-1/')),
    url(r'^berita-2/',RedirectView.as_view(url='/status/berita-2/')),
    url(r'^kebakaran-1/',RedirectView.as_view(url='/status/kebakaran-1/')),
    url(r'^banjir-1/',RedirectView.as_view(url='/status/banjir-1/')),
    url(r'^cuaca-ekstrim-1/',RedirectView.as_view(url='/status/cuaca-ekstrim-1/')),
    
]
